# README

```
./gradlew check bootRun
```

Sorry, due to lack of time, I didn't have time for proper documentation.

* Choice of approach
    * This is a both relationship (line/stop/connection) and multi-dimension (stop X & Y) querying problem
    * What we need is to match by X, Y and time
        * But it could evolve to finding bounding box, or could evolve in scale, or could evolve in relationships, etc.
    * I could have created a normal multi-map lookup strucure, optimized for each query
    * But at this point, makes more sense to use _some_ database
        * First, spent some time making the repositories powered by Lucene
        * Finally decided to use an in-memory SQL database (h2/hsqldb/sqlite)

* Choice of stack (plumbing)
    * Again, what we usually need are the following (at least for Java)
        * Dependency injection (easily manage multiple object instances in the graph without having to manually deal with passing around object references and relationships, override any hierarchy of the DI graph in tests without needing too many nested mocks, etc)
        * A way to configure properties (ideally type-safe so that it can be stubbed in tests like a normal object, instead of having keys scattered everywhere and having to deal with string qualifiers/markers)
        * A way to query the database (annotation-driven or ORM or anything)
        * A way to register multiple handlers and run a server
        * And of course, for production safety, other stuff like health, introspection, logging, etc. (note: adding spring actuator and health indicators/metrics are easy, but I haven't yet)
    * There are multiple ways to reach that
        * Plain Java + JDBC + Java Properties + Java built-in HTTP server
        * Guice + Typesafe Config + jOOQ + Jersey
        * Dagger + Owner + JDBI + Java HTTP Server
        * Dropwizard
        * Spring
        * Play
        * yadda yadda
    * These "flavors" are usually what cause so many discussions
    * But meh, it's a matter of opinion, any of abovesaid/notsaid flavors will work
    * I'm going with old fashioned spring not because I have any preference for it, but just because I don't want to confuse the reader/reviewer
        * I usually prefer turning off most of Spring-magic (specially AutoConfiguration)
        * e.g. I usually pick configuration enabled in /actuator/conditions and hardcode them using `@ImportAutoConfiguration`
        * It helps for faster boot, but then more importantly, it prevents random stuff happening when adding dependencies
        * Unfortunately due to time constraints, I have not been able to do it so left it at that

* Choice of testing
    * Crucial piece of the solution
    * Number 1 requirement of tests is "safety" - aka sleep well in the night
        * Fast turnaround times is nice
        * But when it comes to speed vs safety - safety trumps for tests
    * I'm going with mostly Integration tests at the beginning, because I want to make sure the end result is fine in discovery/high change phase
    * I wanted to add cucumber, but didn't have time to do so
    * I don't have any preferences as long as things are well tested and scenarios are covered

* Backlog due to lack of time
    * Didn't add indexes yet to the lookup structures, still doing full table scans
    * Wanted to remove EnableAutoConfiguration and only use ImportAutoConfiguration with specific modules to reduce bootup and testing time
    * Wanted to add cucumber BDD
    * Wanted to add in the usual defaults, such as automated code formatting, linting, swagger, etc.
    * Didn't cleanup some stuff left behind, like the useless health test, Clock (we're dealing with localtime so clock not required at this point), etc.
