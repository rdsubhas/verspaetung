package com.verspaetung.api;

import com.verspaetung.model.Connection;
import com.verspaetung.model.Line;
import com.verspaetung.model.Stop;
import com.verspaetung.repository.ConnectionRepository;
import com.verspaetung.repository.LineRepository;
import com.verspaetung.repository.StopRepository;
import com.verspaetung.test.Seed;
import com.verspaetung.test.TestApplication;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebFlux;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import javax.persistence.EntityManager;
import java.time.LocalTime;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureWebFlux
public class LineControllerTest {

    @Autowired
    EntityManager entityManager;

    @Autowired
    LineRepository lineRepository;

    @Autowired
    StopRepository stopRepository;

    @Autowired
    ConnectionRepository connectionRepository;

    @Autowired
    WebTestClient webClient;

    Seed seed = new Seed();

    @After
    public void tearDown() {
        entityManager.clear();
    }

    @Test
    public void testLines() {
        Line line = lineRepository.save(seed.line().build());
        Stop stop = stopRepository.save(seed.stop().build());
        Connection connection = connectionRepository.save(seed.connection()
                .id(new Connection.ConnectionId(line.getId(), stop.getId()))
                .build());

        String time = LocalTime.ofSecondOfDay(connection.getDayOffsetSeconds()).toString();
        List<Line> actual = webClient.get().uri("/lines?x={x}&y={y}&timestamp={timestamp}", stop.getX(), stop.getY(), time)
                .exchange()
                .expectBody(new ParameterizedTypeReference<List<Line>>() {
                })
                .returnResult()
                .getResponseBody();

        assertThat(actual, hasItems(line));
    }

    @Test
    public void testLine() {
        Line expected = seed.line().build();
        lineRepository.save(expected);

        Line actual = webClient.get().uri("/lines/{name}", expected.getName())
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBody(Line.class)
                .returnResult()
                .getResponseBody();

        assertThat(actual, is(expected));
    }

}
