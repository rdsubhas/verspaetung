package com.verspaetung.test;


import com.verspaetung.Application;
import com.verspaetung.loader.DataLoader;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@ComponentScan(basePackages = "com.verspaetung", lazyInit = true)
@ActiveProfiles("test")
@ContextConfiguration
public class TestApplication extends Application {

    @MockBean
    DataLoader dataLoader;

}
