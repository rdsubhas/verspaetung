package com.verspaetung.test;

import com.verspaetung.model.Connection;
import com.verspaetung.model.Line;
import com.verspaetung.model.Stop;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Delegate;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;

import java.time.Clock;
import java.util.Random;
import java.util.UUID;

/**
 * Provides pre-filled message objects for testing
 *   (not specifically bound to any test case)
 * Returns builders -> override attributes in tests where possible
 * Deterministic -> produces same messages given same clock and random seed
 */
@RequiredArgsConstructor
public class Seed {

    private final Clock clock;

    @Delegate
    private final Random random;

    public Seed() {
        this(Clock.systemUTC(), new Random());
    }

    public byte[] nextBytes(int size) {
        byte[] b = new byte[128];
        nextBytes(b);
        return b;
    }

    public String string() {
        return UUID.nameUUIDFromBytes(nextBytes(128)).toString();
    }

    public Line.LineBuilder line() {
        return Line.builder()
                .id(nextLong())
                .name(string());
    }

    public Stop.StopBuilder stop() {
        return Stop.builder()
                .id(nextLong())
                .x(nextInt())
                .y(nextInt());
    }

    public Connection.ConnectionBuilder connection() {
        return Connection.builder()
                .dayOffsetSeconds(nextInt(24 * 60 * 60));
    }

    public Resource csv(String... lines) {
        return new ByteArrayResource(("---\n" + String.join("\n", lines)).getBytes());
    }

}
