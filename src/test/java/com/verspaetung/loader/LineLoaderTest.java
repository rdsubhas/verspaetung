package com.verspaetung.loader;

import com.verspaetung.repository.LineRepository;
import com.verspaetung.test.Seed;
import com.verspaetung.test.TestApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
@DataJpaTest
public class LineLoaderTest {

    Seed seed = new Seed();

    @Autowired
    LineRepository repository;

    @Test
    public void testNoRows() throws IOException {
        Resource resource = seed.csv();
        new LineLoader(resource, repository).load();
        assertThat(repository.count(), is(0L));
    }

    @Test
    public void testCorrectRow() throws IOException {
        Resource resource = seed.csv("1,name1");
        new LineLoader(resource, repository).load();
        assertThat(repository.count(), is(1L));
        assertThat(repository.findById(1L).get().getId(), is(1L));
        assertThat(repository.findById(1L).get().getName(), is("name1"));
    }

    @Test
    public void testOverlappingRow() throws IOException {
        Resource resource = seed.csv("1,name1", "1,name2");
        new LineLoader(resource, repository).load();
        assertThat(repository.count(), is(1L));
        assertThat(repository.findById(1L).get().getId(), is(1L));
        assertThat(repository.findById(1L).get().getName(), is("name2"));
    }

    @Test
    public void testManyRows() throws IOException {
        Resource resource = seed.csv("1,name1", "2,name2");
        new LineLoader(resource, repository).load();
        assertThat(repository.count(), is(2L));
        assertThat(repository.findById(1L).get().getId(), is(1L));
        assertThat(repository.findById(1L).get().getName(), is("name1"));
        assertThat(repository.findById(2L).get().getId(), is(2L));
        assertThat(repository.findById(2L).get().getName(), is("name2"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotEnoughColumns() throws IOException {
        Resource resource = seed.csv("1");
        new LineLoader(resource, repository).load();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadId() throws IOException {
        Resource resource = seed.csv("foo,bar");
        new LineLoader(resource, repository).load();
    }

}
