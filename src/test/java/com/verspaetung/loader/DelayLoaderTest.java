package com.verspaetung.loader;

import com.verspaetung.model.Line;
import com.verspaetung.repository.LineRepository;
import com.verspaetung.test.Seed;
import com.verspaetung.test.TestApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
@DataJpaTest
public class DelayLoaderTest {

    Seed seed = new Seed();

    @Autowired
    LineRepository repository;

    @Test
    public void testNoRows() throws IOException {
        Resource resource = seed.csv();
        new DelayLoader(resource, repository).load();
        assertThat(repository.count(), is(0L));
    }

    @Test
    public void testCorrectRow() throws IOException {
        Line line = new Line(1L, "test1", 999);
        repository.save(line);

        Resource resource = seed.csv("test1,0");
        new DelayLoader(resource, repository).load();
        assertThat(repository.findById(1L).get().getDelayOffsetSeconds(), is(0L));
    }

    @Test
    public void testNormalizeMinutesToSeconds() throws IOException {
        Line line = new Line(1L, "test1", 0);
        repository.save(line);

        Resource resource = seed.csv("test1,1");
        new DelayLoader(resource, repository).load();
        assertThat(repository.findById(1L).get().getDelayOffsetSeconds(), is(60L));
    }

    @Test
    public void testOverlappingRows() throws IOException {
        Line line = new Line(1L, "test1", 999);
        repository.save(line);

        Resource resource = seed.csv("test1,1", "test1,0");
        new DelayLoader(resource, repository).load();
        assertThat(repository.findById(1L).get().getDelayOffsetSeconds(), is(0L));
    }

    @Test
    public void testManyRows() throws IOException {
        Line line1 = new Line(1L, "test1", 999);
        Line line2 = new Line(2L, "test2", 999);
        repository.save(line1);
        repository.save(line2);

        Resource resource = seed.csv("test1,1", "test2,2");
        new DelayLoader(resource, repository).load();
        assertThat(repository.findById(1L).get().getDelayOffsetSeconds(), is(60L));
        assertThat(repository.findById(2L).get().getDelayOffsetSeconds(), is(120L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMissingLineId() throws IOException {
        Resource resource = seed.csv("1,0");
        new DelayLoader(resource, repository).load();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidOffset() throws IOException {
        repository.save(new Line(1L, "test1", 0));
        Resource resource = seed.csv("test1,foobar");
        new DelayLoader(resource, repository).load();
    }

}
