package com.verspaetung.loader;

import com.verspaetung.repository.StopRepository;
import com.verspaetung.test.Seed;
import com.verspaetung.test.TestApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
@DataJpaTest
public class StopLoaderTest {

    Seed seed = new Seed();

    @Autowired
    StopRepository repository;

    @Test
    public void testNoRow() throws IOException {
        Resource resource = seed.csv();
        new StopLoader(resource, repository).load();
        assertThat(repository.count(), is(0L));
    }

    @Test
    public void testCorrectRow() throws IOException {
        Resource resource = seed.csv("1,1,2");
        new StopLoader(resource, repository).load();
        assertThat(repository.count(), is(1L));
        assertThat(repository.findById(1L).get().getId(), is(1L));
        assertThat(repository.findById(1L).get().getX(), is(1L));
        assertThat(repository.findById(1L).get().getY(), is(2L));
    }

    @Test
    public void testOverlappingRows() throws IOException {
        Resource resource = seed.csv("1,1,2", "1,3,4");
        new StopLoader(resource, repository).load();
        assertThat(repository.count(), is(1L));
        assertThat(repository.findById(1L).get().getId(), is(1L));
        assertThat(repository.findById(1L).get().getX(), is(3L));
        assertThat(repository.findById(1L).get().getY(), is(4L));
    }

    @Test
    public void testMultipleRows() throws IOException {
        Resource resource = seed.csv("1,1,2", "2,3,4");
        new StopLoader(resource, repository).load();
        assertThat(repository.count(), is(2L));
        assertThat(repository.findById(1L).get().getId(), is(1L));
        assertThat(repository.findById(1L).get().getX(), is(1L));
        assertThat(repository.findById(1L).get().getY(), is(2L));
        assertThat(repository.findById(2L).get().getId(), is(2L));
        assertThat(repository.findById(2L).get().getX(), is(3L));
        assertThat(repository.findById(2L).get().getY(), is(4L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotEnoughColumns() throws IOException {
        Resource resource = seed.csv("1");
        new StopLoader(resource, repository).load();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadId() throws IOException {
        Resource resource = seed.csv("foo,1,2");
        new StopLoader(resource, repository).load();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadX() throws IOException {
        Resource resource = seed.csv("1,foo,2");
        new StopLoader(resource, repository).load();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadY() throws IOException {
        Resource resource = seed.csv("1,1,foo");
        new StopLoader(resource, repository).load();
    }
}
