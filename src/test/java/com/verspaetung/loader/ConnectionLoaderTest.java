package com.verspaetung.loader;

import com.verspaetung.model.Connection;
import com.verspaetung.repository.ConnectionRepository;
import com.verspaetung.test.Seed;
import com.verspaetung.test.TestApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
@DataJpaTest
public class ConnectionLoaderTest {

    Seed seed = new Seed();

    @Autowired
    ConnectionRepository repository;

    @Test
    public void testNoRows() throws IOException {
        Resource resource = seed.csv();
        new ConnectionLoader(resource, repository).load();
        assertThat(repository.count(), is(0L));
    }

    @Test
    public void testCorrectRow() throws IOException {
        Resource resource = seed.csv("1,1,00:00:00");
        new ConnectionLoader(resource, repository).load();
        assertThat(repository.count(), is(1L));
        Connection connection = repository.findById(new Connection.ConnectionId(1L, 1L)).get();
        assertThat(connection.getDayOffsetSeconds(), is(0L));
    }

    @Test
    public void testOverlappingRows() throws IOException {
        Resource resource = seed.csv("1,1,00:00:00", "1,1,00:00:01");
        new ConnectionLoader(resource, repository).load();
        assertThat(repository.count(), is(1L));

        Connection conn = repository.findById(new Connection.ConnectionId(1L, 1L)).get();
        assertThat(conn.getDayOffsetSeconds(), is(1L));
    }

    @Test
    public void testManyRows() throws IOException {
        Resource resource = seed.csv("1,1,00:00:00", "2,2,00:00:00");
        new ConnectionLoader(resource, repository).load();
        assertThat(repository.count(), is(2L));

        Connection conn1 = repository.findById(new Connection.ConnectionId(1L, 1L)).get();
        assertThat(conn1.getDayOffsetSeconds(), is(0L));

        Connection conn2 = repository.findById(new Connection.ConnectionId(2L, 2L)).get();
        assertThat(conn2.getDayOffsetSeconds(), is(0L));
    }

    @Test
    public void testConvertLocalTimeToSeconds() throws IOException {
        Resource resource = seed.csv("1,1,12:00:00");
        new ConnectionLoader(resource, repository).load();
        Connection conn = repository.findById(new Connection.ConnectionId(1L, 1L)).get();
        assertThat(conn.getDayOffsetSeconds(), is(12L * 60L * 60L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotEnoughColumns() throws IOException {
        Resource resource = seed.csv("1");
        new ConnectionLoader(resource, repository).load();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadLineId() throws IOException {
        Resource resource = seed.csv("foo,1,00:00:00");
        new ConnectionLoader(resource, repository).load();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadStopId() throws IOException {
        Resource resource = seed.csv("1,foo,00:00:00");
        new ConnectionLoader(resource, repository).load();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadLocalTime() throws IOException {
        Resource resource = seed.csv("1,1,25:00:00");
        new ConnectionLoader(resource, repository).load();
    }

}
