package com.verspaetung.api;

import com.verspaetung.model.Line;
import com.verspaetung.repository.LineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalTime;

@RestController
@RequestMapping("/lines")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LineController {

    private final LineRepository lineRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Iterable<Line> getLines(@RequestParam("x") Long x, @RequestParam("y") Long y, @RequestParam("timestamp") String timestamp) {
        LocalTime time = LocalTime.parse(timestamp);
        return lineRepository.findByChallenge(x, y, time.toSecondOfDay());
    }

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    public Line getLines(@PathVariable("name") String name) {
        return lineRepository.findByName(name)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

}
