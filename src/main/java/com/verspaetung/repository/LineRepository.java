package com.verspaetung.repository;

import com.verspaetung.model.Line;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface LineRepository extends CrudRepository<Line, Long> {

    Optional<Line> findByName(String name);

    @Query(nativeQuery = true,
            value = "SELECT l.* FROM lines l " +
                    "INNER JOIN connections c ON c.line_id = l.id " +
                    "INNER JOIN stops s ON s.id = c.stop_id " +
                    "WHERE s.x = :x " +
                    "AND s.y = :y " +
                    "AND (c.day_offset_seconds + l.delay_offset_seconds) = :seconds")
    Iterable<Line> findByChallenge(@Param("x") long x, @Param("y") long y, @Param("seconds") long seconds);

}
