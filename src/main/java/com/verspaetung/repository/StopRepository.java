package com.verspaetung.repository;

import com.verspaetung.model.Stop;
import org.springframework.data.repository.CrudRepository;

public interface StopRepository extends CrudRepository<Stop, Long> {
}
