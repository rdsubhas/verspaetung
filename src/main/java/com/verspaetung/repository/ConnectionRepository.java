package com.verspaetung.repository;

import com.verspaetung.model.Connection;
import org.springframework.data.repository.CrudRepository;

public interface ConnectionRepository extends CrudRepository<Connection, Connection.ConnectionId> {
}
