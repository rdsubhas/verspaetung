package com.verspaetung.loader;

import com.google.common.annotations.VisibleForTesting;
import com.opencsv.CSVParser;
import com.verspaetung.config.DataConfig;
import com.verspaetung.model.Connection;
import com.verspaetung.repository.ConnectionRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalTime;

@Slf4j
@Component
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class ConnectionLoader {

    private static final CSVParser PARSER = new CSVParser();

    private final Resource resource;
    private final ConnectionRepository repository;

    @Autowired
    public ConnectionLoader(ApplicationContext context, DataConfig dataConfig, ConnectionRepository repository) {
        this(context.getResource(dataConfig.getStopTimesCsv()), repository);
    }

    @VisibleForTesting
    void load() throws IOException {
        log.info("Loading connections from {}", resource);
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()))) {
            String currentLine = reader.readLine();
            while ((currentLine = reader.readLine()) != null) {
                repository.save(read(currentLine));
            }
        }
    }

    private Connection read(String line) {
        try {
            String[] columns = PARSER.parseLine(line);
            return Connection.builder()
                    .id(new Connection.ConnectionId(Long.parseLong(columns[0]), Long.parseLong(columns[1])))
                    .dayOffsetSeconds(LocalTime.parse(columns[2]).toSecondOfDay())
                    .build();
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format("Connection is invalid: %s", line));
        }
    }

}
