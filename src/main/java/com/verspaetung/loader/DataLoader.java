package com.verspaetung.loader;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Profile("!test")
public class DataLoader implements ApplicationRunner {

    private final LineLoader lineLoader;
    private final DelayLoader delayLoader;
    private final StopLoader stopLoader;
    private final ConnectionLoader connectionLoader;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        lineLoader.load();
        delayLoader.load();
        stopLoader.load();
        connectionLoader.load();
    }

}
