package com.verspaetung.loader;

import com.google.common.annotations.VisibleForTesting;
import com.opencsv.CSVParser;
import com.verspaetung.config.DataConfig;
import com.verspaetung.model.Stop;
import com.verspaetung.repository.StopRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Slf4j
@Component
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class StopLoader {

    private static final CSVParser PARSER = new CSVParser();

    private final Resource resource;
    private final StopRepository repository;

    @Autowired
    public StopLoader(ApplicationContext context, DataConfig dataConfig, StopRepository repository) {
        this(context.getResource(dataConfig.getStopsCsv()), repository);
    }

    @VisibleForTesting
    void load() throws IOException {
        log.info("Loading stops from {}", resource);
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()))) {
            String currentLine = reader.readLine();
            while ((currentLine = reader.readLine()) != null) {
                repository.save(read(currentLine));
            }
        }
    }

    private Stop read(String line) {
        try {
            String[] columns = PARSER.parseLine(line);
            return Stop.builder()
                    .id(Long.parseLong(columns[0]))
                    .x(Long.parseLong(columns[1]))
                    .y(Long.parseLong(columns[2]))
                    .build();
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format("Stop is invalid: %s", line));
        }
    }

}
