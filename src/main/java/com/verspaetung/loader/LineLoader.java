package com.verspaetung.loader;

import com.google.common.annotations.VisibleForTesting;
import com.opencsv.CSVParser;
import com.verspaetung.config.DataConfig;
import com.verspaetung.model.Line;
import com.verspaetung.repository.LineRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Slf4j
@Component
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class LineLoader {

    private static final CSVParser PARSER = new CSVParser();

    private final Resource resource;
    private final LineRepository repository;

    @Autowired
    public LineLoader(ApplicationContext context, DataConfig dataConfig, LineRepository repository) {
        this(context.getResource(dataConfig.getLinesCsv()), repository);
    }

    @VisibleForTesting
    void load() throws IOException {
        log.info("Loading lines from {}", resource);
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()))) {
            String currentLine = reader.readLine();
            while ((currentLine = reader.readLine()) != null) {
                repository.save(read(currentLine));
            }
        }
    }

    private Line read(String line) {
        try {
            String[] columns = PARSER.parseLine(line);
            return Line.builder()
                    .id(Long.parseLong(columns[0]))
                    .name(columns[1])
                    .build();
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format("Line is invalid: %s", line));
        }
    }

}
