package com.verspaetung.model;

import lombok.*;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Entity
@Table(name = "connections")
public class Connection {

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @EqualsAndHashCode
    public static class ConnectionId implements Serializable {
        private Long lineId;
        private Long stopId;
    }

    @EmbeddedId
    private ConnectionId id;
    private long dayOffsetSeconds;

}
