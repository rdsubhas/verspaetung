package com.verspaetung.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Entity
@Table(name = "lines")
public class Line {

    @Id
    private Long id;

    @Column(unique = true)
    private String name;

    private long delayOffsetSeconds;

}
