package com.verspaetung.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DataConfig {
    private String linesCsv;
    private String stopsCsv;
    private String delaysCsv;
    private String stopTimesCsv;
}
