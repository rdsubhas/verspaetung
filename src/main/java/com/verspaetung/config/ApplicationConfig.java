package com.verspaetung.config;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
@ConfigurationProperties
public class ApplicationConfig {

    private DataConfig data;

    @Bean
    public DataConfig getData() {
        return data;
    }

}
